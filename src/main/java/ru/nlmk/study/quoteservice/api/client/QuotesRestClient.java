package ru.nlmk.study.quoteservice.api.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.nlmk.study.quoteservice.model.QuoteWrapper;

@Slf4j
@Service
public class QuotesRestClient {

    private final RestTemplate restTemplate;

    public QuotesRestClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Value("${quote.api.url}")
    private String quoteApiUrl;

    public QuoteWrapper getQuote() {
        log.info("requesting quote from {}", quoteApiUrl);

        ResponseEntity<QuoteWrapper> response = restTemplate.getForEntity(quoteApiUrl, QuoteWrapper.class);

        QuoteWrapper quoteWrapper = response.getBody();

        if (quoteWrapper == null) {
            log.error("unable to get response");
        } else {
            log.info("received response with result: {}", quoteWrapper.getMessage());
        }

        return quoteWrapper;
    }
}

package ru.nlmk.study.quoteservice.messaging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.nlmk.study.quoteservice.model.Quote;

@Slf4j
@Component
public class QuoteSender {

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Value("${spring.rabbitmq.routingKey}")
    private String routingKey;

    private final RabbitTemplate rabbitTemplate;

    public QuoteSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(Quote quote) {
        rabbitTemplate.convertAndSend(exchange, routingKey, quote);
        log.info("sent quote to: {} with routingKey: {}", exchange, routingKey);
    }
}

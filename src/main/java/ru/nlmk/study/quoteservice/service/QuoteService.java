package ru.nlmk.study.quoteservice.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nlmk.study.quoteservice.api.client.QuotesRestClient;
import ru.nlmk.study.quoteservice.messaging.QuoteSender;
import ru.nlmk.study.quoteservice.model.Quote;
import ru.nlmk.study.quoteservice.model.QuoteWrapper;
import ru.nlmk.study.quoteservice.repository.QuoteRepository;

@Service
public class QuoteService {

    private final QuotesRestClient quotesClient;
    private final QuoteRepository quoteRepository;
    private final QuoteSender quoteSender;

    public QuoteService(QuotesRestClient quotesClient, QuoteRepository quoteRepository, QuoteSender quoteSender) {
        this.quotesClient = quotesClient;
        this.quoteRepository = quoteRepository;
        this.quoteSender = quoteSender;
    }

    @Scheduled(fixedRate = 60 * 1000)
    @Transactional
    public void quoteSending() {
        QuoteWrapper quoteWrapper = quotesClient.getQuote();
        Quote quote = quoteWrapper.getQuotes().get(0);

        quoteRepository.save(quote);

        quoteSender.sendMessage(quote);
    }
}

package ru.nlmk.study.quoteservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nlmk.study.quoteservice.model.Quote;

public interface QuoteRepository extends JpaRepository<Quote, Long> {
}

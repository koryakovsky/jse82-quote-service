package ru.nlmk.study.quoteservice.model;

import lombok.Data;

import java.util.List;

@Data
public class QuoteWrapper {

    private String status;

    private String message;

    private int count;

    private List<Quote> quotes;
}

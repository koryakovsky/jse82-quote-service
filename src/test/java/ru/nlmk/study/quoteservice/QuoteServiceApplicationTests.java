package ru.nlmk.study.quoteservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nlmk.study.quoteservice.api.client.QuotesRestClient;

@SpringBootTest
class QuoteServiceApplicationTests {

	@Autowired
	private QuotesRestClient quotesRestClient;


	@Test
	void contextLoads() {
		quotesRestClient.getQuote();
	}

}
